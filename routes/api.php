<?php
use App\Http\Controllers\Api\EntregadorController;
use App\Http\Controllers\Api\RestauranteController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});



//entregador
Route::get('entregadores', 'App\\Http\\Controllers\\Api\\EntregadorController@getAllEntregadores');
Route::get('entregador/{id}', 'App\\Http\\Controllers\\Api\\EntregadorController@getEntregador');
Route::post('entregador', 'App\\Http\\Controllers\\Api\\EntregadorController@createEntregador');
Route::put('entregador/{id}', 'App\\Http\\Controllers\\Api\\EntregadorController@updateEntregador');
Route::delete('entregador/{id}','App\\Http\\Controllers\\Api\\EntregadorController@deleteEntregador');

//restaurante
Route::get('restaurantes', 'App\\Http\\Controllers\\Api\\RestauranteController@getAllRestaurantes');
Route::get('restaurante/{id}', 'App\\Http\\Controllers\\Api\\RestauranteController@getRestaurante');
Route::post('restaurante', 'App\\Http\\Controllers\\Api\\RestauranteController@createRestaurante');
Route::put('restaurante/{id}', 'App\\Http\\Controllers\\Api\\RestauranteController@updateRestaurante');
Route::delete('restaurante/{id}','App\\Http\\Controllers\\Api\\RestauranteController@deleteRestaurante');
