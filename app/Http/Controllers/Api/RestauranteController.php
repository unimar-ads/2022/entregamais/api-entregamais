<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Restaurante;
use Illuminate\Http\Request;

class RestauranteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllRestaurantes() {
        $restaurante = Restaurante::get()->toJson(JSON_PRETTY_PRINT);
        return response($restaurante, 200);
    }
  
    public function createRestaurante(Request $request) {
        $restaurante = new Restaurante;
        $restaurante->nome = $request->nome;
        $restaurante->endereco = $request->endereco;
        $restaurante->cnpj = $request->cnpj;
        $restaurante->telefone = $request->telefone;
        $restaurante->latb = $request->latb;
        $restaurante->lonb = $request->lonb;
        $restaurante->save();
    
        return response()->json([
            "message" => "restaurante record created"
        ], 201);
    }
  
    public function getRestaurante($id) {
        if (Restaurante::where('id', $id)->exists()) {
            $restaurante = Restaurante::where('id', $id)->get()->toJson(JSON_PRETTY_PRINT);
            return response($restaurante, 200);
          } else {
            return response()->json([
              "message" => "restaurante not found"
            ], 404);
        }
    }
  
    public function updateRestaurante(Request $request, $id) {
        if (Restaurante::where('id', $id)->exists()) {
            $restaurante = Restaurante::find($id);
    
            $restaurante->nome = is_null($request->nome) ? $restaurante->nome : $request->nome;
            $restaurante->endereco = is_null($request->endereco) ? $restaurante->endereco : $request->endereco;
            $restaurante->cnpj = is_null($request->cnpj) ? $restaurante->cnpj : $request->cnpj;
            $restaurante->telefone = is_null($request->telefone) ? $restaurante->telefone : $request->telefone;
            $restaurante->latb = is_null($request->latb) ? $restaurante->latb : $request->latb;
            $restaurante->lonb = is_null($request->lonb) ? $restaurante->lonb : $request->lonb;
            $restaurante->save();
    
            return response()->json([
              "message" => "records updated successfully"
            ], 200);
          } else {
            return response()->json([
              "message" => "Restaurante not found"
            ], 404);
        }   
    }   
  
    public function deleteRestaurante ($id) {
        if(Restaurante::where('id', $id)->exists()) {
            $restaurante = Restaurante::find($id);
            $restaurante->delete();
    
            return response()->json([
              "message" => "records deleted"
            ], 202);
          } else {
            return response()->json([
              "message" => "Restaurante not found"
            ], 404);
        }   
    } 

}
