<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Entregador;
use Illuminate\Http\Request;

class EntregadorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllEntregadores() {
        $entregador = Entregador::get()->toJson(JSON_PRETTY_PRINT);
        return response($entregador, 200);
    }
  
    public function createEntregador(Request $request) {
        $entregador = new Entregador;
        $entregador->nome = $request->nome;
        $entregador->endereco = $request->endereco;
        $entregador->cpf = $request->cpf;
        $entregador->telefone = $request->telefone;
        $entregador->lata = $request->lata;
        $entregador->lona = $request->lona;
        $entregador->save();
    
        return response()->json([
            "message" => "entregador record created"
        ], 201);
    }
  
    public function getEntregador($id) {
        if (Entregador::where('id', $id)->exists()) {
            $entregador = Entregador::where('id', $id)->get()->toJson(JSON_PRETTY_PRINT);
            return response($entregador, 200);
          } else {
            return response()->json([
              "message" => "Entregador not found"
            ], 404);
        }
    }
  
    public function updateEntregador(Request $request, $id) {
        if (Entregador::where('id', $id)->exists()) {
            $entregador = Entregador::find($id);
    
            $entregador->nome = is_null($request->nome) ? $entregador->nome : $request->nome;
            $entregador->endereco = is_null($request->endereco) ? $entregador->endereco : $request->endereco;
            $entregador->cpf = is_null($request->cpf) ? $entregador->cpf : $request->cpf;
            $entregador->telefone = is_null($request->telefone) ? $entregador->telefone : $request->telefone;
            $entregador->lata = is_null($request->lata) ? $entregador->lata : $request->lata;
            $entregador->lona = is_null($request->lona) ? $entregador->lona : $request->lona;
            $entregador->save();
    
            return response()->json([
              "message" => "records updated successfully"
            ], 200);
          } else {
            return response()->json([
              "message" => "Entregador not found"
            ], 404);
        }   
    }   
  
    public function deleteEntregador ($id) {
        if(Entregador::where('id', $id)->exists()) {
            $entregador = Entregador::find($id);
            $entregador->delete();
    
            return response()->json([
              "message" => "records deleted"
            ], 202);
          } else {
            return response()->json([
              "message" => "Entregador not found"
            ], 404);
        }   
    } 

}
