<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurantes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('endereco');
            $table->biginteger('cnpj');
            $table->integer('telefone');
            $table->double('latb', 17, 15)->nullable();;
            $table->double('lonb', 17, 15)->nullable();;
            $table->timestamps();
            $table->unique('cnpj');
            $table->unique('telefone');
            $table->unique('nome');
            $table->unique('endereco');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurantes');
    }
};
