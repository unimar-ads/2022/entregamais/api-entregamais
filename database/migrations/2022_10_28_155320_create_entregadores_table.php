<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entregadores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('endereco');
            $table->biginteger('cpf');
            $table->integer('telefone');
            $table->double('lata', 17, 15)->nullable(); 
            $table->double('lona', 17, 15)->nullable(); 
            $table->timestamps();
            $table->unique('cpf');
            $table->unique('telefone');

        });
    }
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entregadores');
    }
};
