<?php

namespace Lib;

use Lib\Ponto;


class Frete {
    /**
     * @param Ponto[] $pontos
     */
    function __construct(private array $pontos) {

    }

    function totalDistance() {
        $distance = 0;
        $pontoAnterior = $this->pontos[0];
        foreach($this->pontos as $ponto) {
            $distance += $ponto->distanceTo($pontoAnterior);
            $pontoAnterior = $ponto;
        }
        return $distance;
    }

}
