<?php

namespace Lib;

class Ponto {
        
    function __construct(
        private $longitude = null,
        private $latitude = null,
    ){
        
    }

    function __get($atributo) {
        return $this->$atributo;
    }

    function __set($atributo, $valor) {
        $this->$atributo = $valor;
    }

    function distanceTo(Ponto $b) {
        //R * arccos (sin (lata) * sin (latB) + cos (lata) * cos (latB) * cos (Lona-lonB)) 

        $dist =  (6372.795477598 * acos((sin($this->latitude) * sin($b->latitude)) + (cos($this->latitude) * cos($b->latitude) * cos($this->longitude - $b->longitude))));
        $dist = number_format(deg2rad($dist), 2, '.', '');
        return $dist;

    }

}