<?php

namespace Tests\Lib;

use Lib\Ponto;
use PHPUnit\Framework\TestCase;

class PontoTest extends TestCase
{
    public function test_distance_zero()
    {

        $a = new Ponto(90, 180);
        $b = new Ponto(90, 180);
        $this->assertEquals(0, $a->distanceTo($b));
    }

}
